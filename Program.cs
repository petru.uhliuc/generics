﻿using System;
using System.Collections.Generic;

namespace generics
{
    class Program : IMyInterface<int>
    {
        static void Main(string[] args)
        {

            //ex1
            int[] a = { 1, 2, 3 };
            int[] b = Reverse(ref a);
            foreach (var VAR in b)
            {
                Console.Write(VAR + " ");
            }
            Console.Write("\n");
            //ex2
            var myobject = new MyClass<Program, Program>();
            myobject.Afisare();
            //ex3
            var c = new HashSet<int>(); //nu suporta duplicate
                                        //ex4
            var Customers = new List<Customer>();
            Customer customer = new Customer("Tony", 22);
            customer.Orders.Add(new Order(11, "Billionaire", "05.02.2021"));
            customer.Orders.Add(new Order(12, "Genius", "06.02.2021"));
            customer.Orders.Add(new Order(13, "Philanthropist", "07.02.2021"));
            Customers.Add(customer);
            Customer customer1 = new Customer("Joe", 22);
            customer1.Orders.Add(new Order(21, "Gearless", "06.08.2020"));
            customer1.Orders.Add(new Order(22, "Fearless", "06.08.2020"));
            customer1.Orders.Add(new Order(23, "Nomad", "06.08.2020"));
            Customers.Add(customer1);
            Customer customer2 = new Customer("Dante", 22);
            customer2.Orders.Add(new Order(31, "Devil hunter", "10.08.2021"));
            customer2.Orders.Add(new Order(32, "Dark Knight", "10.08.2021"));
            customer2.Orders.Add(new Order(33, "Demon slayer", "10.10.2021"));
            Customers.Add(customer2);
            var colec = new SortedList<string, List<Order>>();
            foreach (var VARIABLE in Customers)
            {
                colec.Add(VARIABLE.Name, VARIABLE.Orders);
            }
            Console.Write("\n");
            foreach (var VARIABLE in colec)
            {
                Console.Write(VARIABLE.Key + ":");
                foreach (var VARIABLE1 in VARIABLE.Value)
                {
                    Console.Write(VARIABLE1.toString() + " ");
                }
                Console.Write("\n");
            }
            //ex5
            var repository = new LinkedList<Customer>();
            Customer customer3 = new Customer();
            customer3 = customer3.Create("Mihail", "21", null);
            repository.AddFirst(customer3);
        }
        static T[] Reverse<T>(ref T[] inp)
        {
            int nr = inp.Length;
            T[] outp = new T[nr];
            int j = 0;
            for (int i = nr - 1; i >= 0; i--)
            {
                outp[j] = inp[i];
                j++;
            }
            return outp;
        }
    }

    public class MyClass<T1, T2>
    where T1 : class, new()
    where T2 : IMyInterface<int>
    {
        public void Afisare()
        {
            Console.Write(typeof(T1) + " " + typeof(T2));
        }
    }

    public interface IMyInterface<T>
    {

    }

    public class Customer : ICRUD<Customer, string>
    {
        public string Name;
        public int Age;
        public List<Order> Orders;

        public Customer()
        {

        }

        public Customer(string name, int age)
        {
            Name = name;
            Age = age;
            Orders = new List<Order>();
        }

        public Customer Create(string value, string value1, string value2)
        {
            return new Customer(value, Convert.ToInt32(value1));
        }

        public void Delete(ref Customer item)
        {
            item = null;
        }

        public void Read()
        {
            Console.WriteLine(this.Name + " " + this.Age);
            foreach (var VARIABLE in Orders)
            {
                VARIABLE.Read();
            }
        }

        public void Update(string value, string value1, string value2)
        {
            this.Name = value;
            this.Age = Convert.ToInt32(value1);

        }
    }

    public class Order : ICRUD<Order, string>
    {
        public int Number;
        public string Title;
        public string Date;

        public Order(int number, string title, string date)
        {
            Number = number;
            Title = title;
            Date = date;
        }

        public Order Create(string value, string value1, string value2)
        {
            var ord = new Order(Convert.ToInt32(value), value1, value2);
            return ord;
        }

        public void Delete(ref Order item)
        {
            item = null;
        }

        public void Read()
        {
            Console.WriteLine(this.toString());
        }

        public string toString()
        {
            return Number.ToString() + " " + Title + " " + Date;
        }

        public void Update(string value, string value1, string value2)
        {
            this.Number = Convert.ToInt32(value);
            this.Date = value2;
            this.Title = value1;
        }
    }

    public interface ICRUD<T, T1>
    {
        T Create(T1 value, T1 value1, T1 value2);
        void Read();
        void Update(T1 value, T1 value1, T1 value2);
        void Delete(ref T item);
    }
}
